import { Component, OnInit } from '@angular/core';
import { Meal } from "../models/meal.model";
import { Router } from "@angular/router";
import { ListCounterService } from "../core/list-counter.service";

@Component({
  selector: 'app-meal',
  templateUrl: './meal.component.html',
  styleUrls: ['./meal.component.scss']
})
export class MealComponent implements OnInit {

  public mealList: Meal[] = [];
  public itemList: any = [];
  public isItemListEmpty = false;
  public isListActive = false;

  constructor(
    private listCounterService: ListCounterService,
    private router: Router,
  ) {
    this.initializeMeal();
  }

  ngOnInit() {

  }

  public initializeMeal() {
    this.mealList.push(
      new Meal('Борщ', 'Первые блюда', {'Картофель':200, 'Свекла':200, 'Свинина':300}, 'borscht.jpg', '60 минут'),
      new Meal('Суп', 'Первые блюда', {'Картофель':200, 'Морковь':100, 'Свинина':200}, 'soup.jpg', '30 минут'),
      new Meal('Рассольник', 'Первые блюда', {'Картофель':200, 'Огурцы':200, 'Говядина':150}, 'cucumber-soup.jpg', '45 минут'),
      new Meal('Плов', 'Вторые блюда', {'Рис':200, 'Баранина':200, 'Морковь':150}, '1.jpg', '45 минут'),
      new Meal('Мясо по-французски', 'Вторые блюда', {'Свинина':200, 'Сыр':200, 'Лук':150}, '2.jpg', '45 минут'),
      new Meal(' Фаршированные баклажаны', 'Вторые блюда', {'Баклажан':200, 'Лук':200, 'Чеснок':150}, '3.jpg', '45 минут'),
    );
  }

  public addItem(index) {
    let item = this.mealList[index];

    this.itemList.push(item);
    this.mealList.splice(index, 1);

    if (this.mealList.length === 0){
      this.isItemListEmpty = true;
    }

    this.isButtonListActive();
    this.listCounterService.prepareList(this.itemList);
  }

  public removeItem(index) {
    let item = this.itemList[index];

    this.itemList.splice(index, 1);
    this.mealList.push(item);

    if (this.mealList.length){
      this.isItemListEmpty = false;
    }

    this.isButtonListActive();
  }

  public goToList() {

    if (this.itemList.length > 0) {
      this.router.navigate(['list']);
    }
  }

  public isButtonListActive() {
    (this.itemList.length > 0) ? this.isListActive = true : this.isListActive = false;
  }
}
