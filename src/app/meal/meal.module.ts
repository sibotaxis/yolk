import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ItemComponent } from './components/item/item.component';
import { MealComponent } from "./meal.component";

@NgModule({
  declarations: [
    ItemComponent,
    MealComponent,
  ],
  imports: [
    CommonModule,
  ]
})
export class MealModule { }
