import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListComponent } from './list/list.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { MealModule } from "./meal/meal.module";

@NgModule({
  declarations: [
    AppComponent,
    ListComponent,
    ErrorPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MealModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
