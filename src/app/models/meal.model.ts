export class Meal {
  name: string;
  type: string;
  ingredients: {};
  image: string;
  time: string;

  constructor(name: string, type: string, ingredients: {}, image: string, time: string, ) {
    this.name = name;
    this.type = type;
    this.ingredients = ingredients;
    this.image = image;
    this.time = time;
  }
}
