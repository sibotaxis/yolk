import { Component, OnInit } from '@angular/core';
import {ListCounterService} from "../core/list-counter.service";


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  objectKeys = Object.keys;
  items = JSON.parse(localStorage.getItem('list'));


  constructor(
    private listCounterService: ListCounterService,
  ) { }

  ngOnInit() {
    console.log(this.listCounterService.list);
  }



}
