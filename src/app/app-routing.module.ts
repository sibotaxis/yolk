import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MealComponent} from "./meal/meal.component";
import {ListComponent} from "./list/list.component";
import {ErrorPageComponent} from "./error-page/error-page.component";


const routes: Routes = [
  {path: '', component: MealComponent},
  {path: 'list', component: ListComponent},
  {path: '404', component: ErrorPageComponent},
  { path: '**', redirectTo: '404' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
