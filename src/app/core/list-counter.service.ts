import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ListCounterService {
  public list: any;

  public getArray() {
    return this.list;
  }

  public prepareList(array) {
    let list = {};

    array.forEach(function (item) {
      for (let key in item.ingredients) {

        if (key in list) {
          list[key] = list[key] + item.ingredients[key];
        } else {
          list[key] = item.ingredients[key];
        }
      }
    });

    this.list = list;
    localStorage.setItem('list', JSON.stringify(this.list));
  }

  public clearList() {
    this.list = {};
  }

}
