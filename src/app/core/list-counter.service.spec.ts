import {TestBed} from '@angular/core/testing';

import {ListCounterService} from './list-counter.service';

describe('ListCounterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ListCounterService = TestBed.get(ListCounterService);
    expect(service).toBeTruthy();
  });
});
